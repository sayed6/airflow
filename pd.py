import pandas as pd
import numpy as np


df = pd.read_csv('winemag-data-130k-v2.csv')

df.drop('description', inplace=True, axis=1)

df.drop('designation', inplace=True, axis=1)

df.drop('region_1', inplace=True, axis=1)

df.drop('Unnamed: 0', inplace=True, axis=1)

df.drop('region_2', inplace=True, axis=1)

df['price'] = df['price'].fillna(0)

DD = pd.DataFrame(df)

DD.loc[(df['points'] == 90) & (df['price'] >= 11.0), 'price'] = 1

price = DD['price'].sum()

#[df['price']/('points'+price)]

DD["price"] = DD.price/(DD.points+price)

SS = DD[DD.country=='Portugal']

SS.to_csv('new.csv', index=False)



