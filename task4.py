import urllib.request
import tabula as tb
import pandas as pd
import os
import requests
import csv
import time


from selenium import webdriver
from selenium.webdriver.common.by import By
from datetime import date, timedelta, datetime

#get in the url

driver = webdriver.Chrome("/usr/bin/chromedriver")
driver.get("https://www.myetf.com.my/en/MyETF-Series/MyETF-DJIM25/Downloads?cat=DailyFundValues_NAV")
driver.maximize_window()
driver.implicitly_wait(5)
date_tracker = driver.find_elements(By.XPATH,"//div[@class='info']")

#fetch todays date and find previous date

today = date.today()
yesterday = today - timedelta(days=1)
modified_date = yesterday.strftime("%d %b %Y")
print("Today's date is",today)
print("yesterday date is",yesterday)
print("Modified date is",modified_date)

#append dates in list

pdf_date_list = []
time.sleep(1)
for dates in date_tracker:
    pdf_date_list.append(dates.text)
print(pdf_date_list)

#find index number of download button

if yesterday_date_modified in pdf_date_list:
    Index_number = pdf_date_list.index(modified_date)
Index_number = Index_number + 1
print(Index_number)

#click download button based on index number

download_button = driver.find_element(By.XPATH,"//*[@id='collapse10415']/div/ul/li[%d]/div[2]/a"%Index_number).click()
agree_button = driver.find_element(By.XPATH,"//*[@id='DisclaimerDownload']/div/div/div[3]/button[1]")
url = agree_button.click()
time.sleep(3)

#switch to next window or tab and get the url of new tab

Current_window = driver.current_window_handle
parent = driver.window_handles[0]
chld = driver.window_handles[1]
driver.switch_to.window(chld)
currenturl = driver.current_url

#download the pdf file

def download_file(download_url, filename, file_date):
    response = urllib.request.urlopen(download_url)    
    file = open(filename + " " + file_date + ".pdf", 'wb')
    file.write(response.read())
    file.close()
    driver.quit()

#calling function

download_file(currenturl, "latest", yesterday_date_modified)

#Processing the data extracted from pdf

myfile = "latest {}.pdf".format(modified_date)
mytable = tb.read_pdf(myfile, pages = 'all', multiple_tables = True)
listof_values = list(mytable[1].iloc[4])
Dow_Jones_index_value = float(listof_values[4].replace(',',''))
scraped_value = {'NAV per Unit (RM)':float(listof_values[3]),'Dow Jones Islamic Market Malaysia Titans 25 Index':Dow_Jones_index_value}
list_values = list(mytable[2].iloc[1])
price_date = list_values[4]
date = datetime.strptime(price_date, '%d-%b-%y')
modified_price = date.strftime('%Y-%b-%d')
scraped_value['Price As At'] = modified_price
header_list = ['NAV per Unit (RM)','Dow Jones Islamic Market Malaysia Titans 25 Index','Price As At']
print(scraped_value)

#write in the csv file

with open('Latest.csv', 'w') as csv_file:  
    writer = csv.writer(csv_file)
    for k, v in scraped_value.items():
       writer.writerow([k, v])
