import PyPDF2
import csv
from datetime import date, datetime

# extract pdf content

with open('Manulife Multifactor Developed International Index ETF - Hedged Units.pdf','rb') as pdf_file:
rd_pdf = PyPDF2.PdfFileReader(scrap_file)
no_of_pages = rd_pdf.getNumPages()
page = rd_pdf.getPage(0)
content = page.extractText()
print((content))
print(type(content))

# extract nav, outstanding value, date

nav =(content.find("NAV per unit"))
Outstanding_value = (content.find("Units Outstanding"))
print((nav))
y = content[nav:nav+19].replace(',','').split('$')[-1]
print(y)
print((nav))
z= content[Outstanding_value:Outstanding_value+24].split(':')[-1]
print(Outstanding_value)
print(z)
total_assets = float(y)*float(z)
print(total_assets)
month= content[nav:nav+38].replace(',','').split()[-1]
day = content[nav:nav+38].replace(',','').split()[-2]
dates= content[nav:nav+38].replace(',','').split()[-3]
as_of_date = month + day + dates
print(month,day,dates)
print(as_of_date)
as_of_date= datetime.strptime(as_of_date, '%Y%d%B').strftime('%Y-%m-%d')
print(as_of_date)

# create dictionary

scraped_value ={'NAV per unit': y, 'as_of_date':as_of_date,'total_assets':total_assets,'Outstanding': Outstanding_value}
print(scraped_value.keys())
print(scraped_value.values())
print()

# create csv file

field_names= ['nav', 'outstanding', 'as_of_date','total_assets']
csv_file = [
{'nav':y, 'outstanding':z, 'as_of_date':as_of_date,'total_assets':total_assets}
]
with open('neww.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=field_names)
    writer.writeheader()
    writer.writerows(csv_file)
print(scraped_value)
pdf_file.close()







